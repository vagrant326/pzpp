﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Linq.Dynamic;
using System.Xml.Serialization;
using System.Globalization;

namespace BazaDanych
{
    /// <summary>
    /// Główna klasa modelująca pojedyńczą, pełną wiadomość
    /// </summary>
    [XmlRoot(ElementName = "item")]
    [Serializable]
    public class Wiadomosc
    {
        public int WiadomoscId { get; set; }

        // ReSharper disable InconsistentNaming
        [XmlElement(ElementName = "title")]
        public string tytul { get; set; }


        [Column(TypeName = "varchar(MAX)")]
        public string kanal { get; set; }


        [XmlElement(ElementName = "link")]
        [Column(TypeName = "varchar(MAX)")]
        public string link { get; set; }


        [XmlElement(ElementName = "description")]
        [Column(TypeName = "varchar(MAX)")]
        public string tresc { get; set; }


        [XmlElement(ElementName = "category")]
        public string kategoria { get; set; }

        /// <summary>
        /// Pole pośredniczące pomiędzy datą, zapisaną jako tekst, w kanale RSS a obiektem DateTime w bazie danych
        /// </summary>
        [NotMapped]
        [XmlElement(ElementName = "pubDate")]
        public string dataString
        {
            get
            {
                return data.ToLongDateString();
            }
            set
            {
                try
                {
                    // Spróbuj wczytać datę w formacie RFC 1123
                    data = DateTime.ParseExact(value, "R", null);
                }
                // Jeśli się nie uda (niektóre daty są zapisane ze zmodyfikowanym znacznikiem strefy czasowej)
                catch (FormatException)
                {
                    //Przypisz strefę czasową GMT i wczytaj ponownie
                    data = DateTime.ParseExact(value.Substring(0, value.Length - 5) + "GMT", "R", null);
                }
            }
        }

        public DateTime data { get; set; }


        [XmlElement(ElementName = "guid")]
        [Column(TypeName = "varchar(MAX)")]
        public string guid { get; set; }


        [Column(TypeName = "text")]
        public string artykul { get; set; }

        [Column(TypeName = "varchar(MAX)")]
        public string zdjecie { get; set; }

        // ReSharper restore InconsistentNaming
    }

    /// <summary>
    /// Klasa wymagana przez Entity Framework
    /// </summary>
    public class WiadomoscContext : DbContext
    {
        public WiadomoscContext():
            base("DBContext")
        {
        }
        public DbSet<Wiadomosc> Wiadomosci { get; set; }
    }

    /// <summary>
    /// Główna klasa wykonująca operacje na bazie danych
    /// </summary>
    public static class Operacje
    {
        /// <summary>
        /// Tekst opisujący wiadomości które nie mają przypisanej żadnej kategorii
        /// </summary>
        public const string brakKategorii = "Bez kategorii";

        /// <summary>
        /// Funkcja zapisująca pojedyńczą wiadomość do bazy danych
        /// </summary>
        /// <param name="wiadomosc">Wiadomość która ma zostać zapisana</param>
        public static void Zapisz(Wiadomosc wiadomosc)
        {
            using (var db = new WiadomoscContext())
            {
                db.Wiadomosci.Add(wiadomosc);
                db.SaveChanges();
            }
        }

        // Pobiera z BazaDanych 
        /// <summary>
        /// Funkcja tworząca listę par [String - nazwa kategorii, String - nazwa kanału w którym występuje]
        /// </summary>
        /// <returns>Lista par [String - nazwa kategorii, String - nazwa kanału w którym występuje]</returns>
        public static List<KeyValuePair<string, string>> OdczytajKategorie()
        {
            // Stwórz nową listę par <string,string>
            List<KeyValuePair<string, string>> kategorie = new List<KeyValuePair<string, string>>();

            using (var db = new WiadomoscContext())
            {
                // Zwróć wszystkie kategorie znajdujące się w bazie
                var zapytanie = from x in db.Wiadomosci
                                group x by x.kategoria into y
                                select y;

                // Zwróć wszystkie kanały znajdujące się w bazie
                var zapytanie2 = from x in db.Wiadomosci
                                 group x by x.kanal into y
                                 select y;

                // Dla każdego kanału
                foreach (var item in zapytanie2)
                {
                    // Dodaj nową parę umożliwiającą wyświetlenie wszystkich wiadomości, w tym tych bez kategorii
                    kategorie.Add(new KeyValuePair<string, string>(brakKategorii, item.First().kanal));
                }

                // Dla każdej grupy wiadomości o danej kategorii
                foreach (var item in zapytanie)
                {
                    // Jeśli kategoria jest pusta
                    if (item.Key == null)
                    {
                        // Pomiń to przejście
                        continue;
                    }

                    // Dla każdej wiadomości w grupie
                    foreach (var item2 in item)
                    {
                        // Jeśli kolekcja nie zawiera jeszcze takiej pary
                        if (!kategorie.Contains(new KeyValuePair<string, string>(item.Key, item2.kanal)))
                        {
                            // Dodaj parę [Kategoria, Kanał w którym występuje
                            kategorie.Add(new KeyValuePair<string, string>(item.Key, item2.kanal));
                        }
                    }
                }
            }
            // Zwróć gotową listę par
            return kategorie;
        }

        /// <summary>
        /// Funkcja pobierająca 30 najnowszych wiadmości z wybranych kanałów
        /// </summary>
        /// <param name="ktore">Lista nazw kanałów z których można wybrać wiadomości</param>
        /// <returns>Lista 30 najnowszych wiadomości z wybranych kanałów</returns>
        public static List<Wiadomosc> WczytajNajnowsze(List<string> ktore)
        {
            // Stwórz pustą listę wiadomości
            List<Wiadomosc> najnowsze = new List<Wiadomosc>();
            using (var db = new WiadomoscContext())
            {
                // Pobierz 30 posortowanych względem daty wiadomości których kanał jest znajduje się na otrzymanej liście
                var dane =  (
                                from x in db.Wiadomosci 
                                where ktore.Contains(x.kanal) 
                                orderby x.data 
                                select x
                            ).Take(30);
                // ReSharper disable once LoopCanBeConvertedToQuery -- odpowiednik LINQ wysypuje błąd
                // Dla każdej pobranej wiadomości
                foreach (var item in dane)
                {
                    // Dodaj do zwracanej listy
                    najnowsze.Add(item);
                }
            }
            // Zwróć gotową listę
            return najnowsze;
        }

        /// <summary>
        /// Funkcja zwracająca listę wiadomości spełniających wszystkie warunki
        /// </summary>
        /// <param name="kategoria">Nazwa kategorii do której mają należeć wiadomości</param>
        /// <param name="poczatek">Minimalna data wiadomości</param>
        /// <param name="koniec">Maksymalna data wiadomości</param>
        /// <param name="kanaly">Lista kanałów do których może należeć wiadomość</param>
        /// <returns>Lista wiadomości o wybranej kategorii, z podanego zakresu dat, z wybranych kanałów</returns>
        public static List<Wiadomosc> WczytajTytuly(string kategoria, DateTime poczatek, DateTime koniec, List<string> kanaly)
        {
            // Ustaw datę końcową na koniec podanego dnia
            koniec = new DateTime(koniec.Year, koniec.Month, koniec.Day, 23, 59, 59);
            

            // Parametrem równań jest wiadomość
            ParameterExpression zmienna = Expression.Parameter(typeof(Wiadomosc), "wiadomosc");

            // Lewą stroną równania jest wiadomosc.kategoria
            Expression lewa = Expression.PropertyOrField(zmienna, "kategoria");

            // Prawą stroną równania jest parametr funkcji - kategoria
            Expression prawa = Expression.Constant(kategoria);

            // Jeśli nie wybrano żadnej kategorii rownanie to lewa != prawa, jeśli kategoria ma znaczenie to lewa == prawa
            Expression rownanie = kategoria == brakKategorii ? Expression.NotEqual(lewa, prawa) : Expression.Equal(lewa, prawa);


            using (var db = new WiadomoscContext())
            {
                // Dane do równania pochodzą z bazy danych
                IQueryable<Wiadomosc> dane = db.Wiadomosci;

                // Zapytanie zwraca wiadomości które spełniają równanie
                MethodCallExpression zapytanie = Expression.Call(typeof(Queryable), "Where", new[] { dane.ElementType },
                    dane.Expression, Expression.Lambda<Func<Wiadomosc, bool>>(rownanie, new[] { zmienna }));

                // Wykonaj zapytanie
                IQueryable<Wiadomosc> zKategorii = dane.Provider.CreateQuery<Wiadomosc>(zapytanie);


                // Lewą stroną równania jest wiadomosc.data
                lewa = Expression.PropertyOrField(zmienna, "data");

                // Prawą stroną równania jest parametr funkcji - poczatek
                prawa = Expression.Constant(poczatek);

                // Równanie to lewa >= prawa
                rownanie = Expression.GreaterThanOrEqual(lewa,prawa);

                // Zapytanie zwraca wiadomości o wybranej kategorii, które spełniają równanie
                zapytanie = Expression.Call(typeof(Queryable), "Where", new[] { zKategorii.ElementType },
                    zKategorii.Expression, Expression.Lambda<Func<Wiadomosc, bool>>(rownanie, new[] { zmienna }));

                // Wykonaj zapytanie
                IQueryable<Wiadomosc> nowszeNiz = zKategorii.Provider.CreateQuery<Wiadomosc>(zapytanie);


                // Lewą stroną równania jest wiadomosc.data
                lewa = Expression.PropertyOrField(zmienna, "data");

                // Prawą stroną równania jest parametr funkcji - koniec
                prawa = Expression.Constant(koniec);

                // Równanie to lewa <= prawa
                rownanie = Expression.LessThanOrEqual(lewa, prawa);

                // Zapytanie zwraca wiadomości o wybranej kategorii, nowsze niż wybrana data, które spełniają równanie
                zapytanie = Expression.Call(typeof(Queryable), "Where", new[] { nowszeNiz.ElementType },
                nowszeNiz.Expression, Expression.Lambda<Func<Wiadomosc, bool>>(rownanie, new[] { zmienna }));

                // Wykonaj zapytanie
                IQueryable<Wiadomosc> starszeNiz = nowszeNiz.Provider.CreateQuery<Wiadomosc>(zapytanie);

                // Zwróc wiadomości o wybranej kategorii, z podanego zakresu day, których kanał znajduje się na żądanej liście
                return starszeNiz.Provider.CreateQuery<Wiadomosc>(zapytanie).Where(x => kanaly.Any(y => y == x.kanal)).ToList();
            }
        }

        /// <summary>
        /// Funkcja sprawdzająca czy wiadomość o podanym GUID istnieje już w bazie
        /// </summary>
        /// <param name="guid">GUID do porównania</param>
        /// <returns>True jeśli znaleziono taką wiadomość, false w przeciwnym wypadku</returns>
        public static bool CzyWiadomoscIstniejeWBazie(string guid)
        {
            using (var db = new WiadomoscContext())
            {
                // Jeśli którakolwiek wiadomość posiada to guid, zwróć true
                return db.Wiadomosci.Any(x => x.guid == guid);
            }
        }

        /// <summary>
        /// Funkcja wykonująca tzw. Cold Query. Sprawdza istnienie i konfiguruje połączenie z bazą. Uruchamiać w oddzielnym wątku.
        /// </summary>
        /// <exception cref="DbUpdateException">Nie udało się nawiązać połączenia z bazą</exception>
        public static void PrzygotujBaze()
        {
            using (var db = new WiadomoscContext())
            {
                // ReSharper disable once UnusedVariable
                try
                {
                    var dane = from x in db.Wiadomosci select x;
                }
                catch (SqlException)
                {
                    // Zły connection string! Dalszy kod sypie aplikację w trybie DEBUG. Tryb RELEASE działa poprawnie.
                    db.Dispose();
                    throw new DbUpdateException();
                }
            }
        }
    }
}
