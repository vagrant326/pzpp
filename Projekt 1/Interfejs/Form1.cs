﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Drawing;
using System.Threading.Tasks;
using System.Globalization;
using System.Windows.Forms;
using System.Linq;
using System.Xml;
using BazaDanych;

// ZMIENIAJCIE SOBIE CONNECTION STRING W BAZADANYCH/APPCONFIG.XML

namespace Projekt_1
{
    public partial class InteriaNewsAgent : Form
    {
        // Zmienna przechowująca referencje do ListBoxa który został użyty do wyboru wyświetlanej wiadomości
        ListBox wybor;
        int poprzedniaZakladka;
        public InteriaNewsAgent()
        {
            InitializeComponent();
            
            // Wczytaj plik konfiguracyjny
            CzytajConfig();

            try
            {
                // Spróbuj wykonać Cold Query. UWAGA! W razie złego connection stringa działa tylko w trybie RELEASE
                Task.Factory.StartNew(Operacje.PrzygotujBaze);
            }
            catch (AggregateException)
            {
                // Obsługa błędu w trybie RELEASE - wypisz komunikat i zamknij aplikację
                MessageBox.Show("Błąd połączenia z bazą danych. Sprawdź connection string w BazaDanych/App.config i Interfejs/App.config! \nAplikacja zostanie zamknięta.", @"Błąd!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(0);
            }

            // Konfiguracja grafiki interfejsu
#if DEBUG
            pictureBox1.InitialImage = Image.FromFile("../../../interia_logo_2_0_0.jpg");
            pictureBox2.InitialImage = Image.FromFile("../../../interia_logo_2_0_0.jpg");
            tabPage1.BackgroundImage = Image.FromFile("../../../dobry3.jpg");
            tabPage2.BackgroundImage = Image.FromFile("../../../dobry3.jpg");
            tabPage3.BackgroundImage = Image.FromFile("../../../dobry3.jpg");
            tabPage5.BackgroundImage = Image.FromFile("../../../dobry3.jpg");
#else
            pictureBox1.InitialImage = Image.FromFile("./interia_logo_2_0_0.jpg");
            pictureBox2.InitialImage = Image.FromFile("./interia_logo_2_0_0.jpg");
            tabPage1.BackgroundImage = Image.FromFile("./dobry3.jpg");
            tabPage2.BackgroundImage = Image.FromFile("./dobry3.jpg");
            tabPage3.BackgroundImage = Image.FromFile("./dobry3.jpg");
            tabPage5.BackgroundImage = Image.FromFile("./dobry3.jpg");

#endif

            //Pobieranie.Pobieranie.NowePobieranieKanalow(); //jest w Pobierz(). Usunąć jeśli wywołanie Pobierz nie jest zakomentarzowane

            // Pobierz wszystkie wiadomości z wszystkich kanałów
            Pobieranie.Pobieranie.Pobierz();

            // Wypełnia listBox1 nazwami kanałów które można w nim wybrać
            WypelnijCheckedListBoxa();


            // Konfiguracja kalendarza
            monthCalendar1.MaxDate = DateTime.Now;
            monthCalendar1.SetSelectionRange(DateTime.Now, DateTime.Now);

            // Ustaw minimalny rozmiar okna
            MinimumSize = new Size(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height+20);

            // Wszystkie tabPage należy wstępnie przygotować
            tabPage1.Tag = false;
            tabPage2.Tag = false;
            tabPage3.Tag = false;
            tabPage5.Tag = false;
        }

        /// <summary>
        /// Funkcja wczytująca plik konfiguracyjny
        /// </summary>
        private static void CzytajConfig()
        {
            XmlDocument plik = new XmlDocument();

            // Wczytaj plik znajdujący się w katalogu głównym
            plik.Load("../../config.xml");

            // Wczytaj węzeł adres
            XmlNode wezel = plik.SelectSingleNode("/config/adres");

            // Jeśli udało się wczytać
            if (wezel != null)
            {
                // Ustaw główny adres z którego będą pobierane kanały
                Pobieranie.Pobieranie.glownyAdresURL = wezel.InnerText;
            }
            else
            {
                // W przeciwnym razie zwróć komunikat o błędzie
                MessageBox.Show(@"Błąd wczytywania konfiguracji!", @"Błąd!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        #region Przyciski Dalej/Wstecz

        /// <summary>
        /// Funkcja dla przycisków "Dalej" na stronach tabControl1. Po kliknięciu wyświetla następną stronę
        /// </summary>
        /// <param name="sender">Przycisk w tabControl1</param>
        /// <param name="e">null</param>
        private void Dalej(object sender, EventArgs e)
        {
            // Jeśli nie zaznaczono żadnego kanału
            if (checkedListBox1.CheckedItems.Count == 0)
            {
                MessageBox.Show(@"Wybierz przynajmniej jedną kategorię!", @"Błąd!",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            // Ustaw stronę na następną
            tabControl1.SelectedIndex++;

            // Wymuś ponowne malowanie tabControl1
            tabControl1.Refresh();    
        }


        /// <summary>
        /// Funkcja dla przycisków "Wstecz" na stronach tabControl1. Po kliknięciu wyświetla poprzednią stronę
        /// </summary>
        /// <param name="sender">Przycisk w tabControl1</param>
        /// <param name="e">null</param>
        private void Wstecz(object sender, EventArgs e)
        {
            // Ustaw stronę na poprzednią
            tabControl1.SelectedIndex--;

            // Wymuś ponowne malowanie tabControl1
            tabControl1.Refresh();
        }
        #endregion

        /// <summary>
        /// Wypełnia listBox1 nazwami kanałów które mogą zostać wybrane
        /// </summary>
        private void WypelnijCheckedListBoxa()
        {
            // Dla każdego kanału
            foreach (var item in Pobieranie.Pobieranie.kanaly)
            {
                // Ustaw nazwę kanału jako etykietę CheckBoxa
                checkedListBox1.Items.Add(item.nazwa);
            }

            // Dostosuj szerokość kolumny elementów
            checkedListBox1.ColumnWidth = Width / 2;
        }

        /// <summary>
        /// Funkcja przygotowująca tabPage1 do wyświetlenia
        /// </summary>
        /// <param name="sender">tabPage1</param>
        /// <param name="e">null</param>
        private void tabPage1_Paint(object sender, PaintEventArgs e)
        {
            // Jeśli tabPage1 nie jest gotowy
            if (!(bool)tabPage1.Tag)
            {
                // Ustalenie rozmiarów interfejsu
                checkedListBox1.Top = 30;
                checkedListBox1.Height = panel2.Height - 50;
                checkedListBox1.ColumnWidth = Screen.PrimaryScreen.Bounds.Width / 4 - 20;

                // tabPage1 jest gotowy
                tabPage1.Tag = true;
            }
        }

        /// <summary>
        /// Funkcja przygotowująca stronę drugą do wyboru filtrów. Uruchamiana tylko jeśli wprowadzono zmiany w listBox1.
        /// </summary>
        /// <param name="sender">auto</param>
        /// <param name="e">null</param>
        private void tabPage2_Paint(object sender, PaintEventArgs e)
        {
            // Jeśli tabPage2 nie jest gotowy
            if (!(bool)tabPage2.Tag)
            {
                // Ustalenie rozmiarów interfejsu
                panel7.Width = Screen.PrimaryScreen.Bounds.Width / 4;
                panel7.Height = Screen.PrimaryScreen.Bounds.Height+10;
                panel7.Left = button2.Right;
                listBox2.Height = panel7.Height - label5.Bottom- 100;
                listBox2.Width = panel7.Width - 5;

                button2.Left = Screen.PrimaryScreen.Bounds.Width - panel7.Width - button2.Width - 30;
                panel7.Left = button2.Right;

                monthCalendar1.Height = panel3.Height - 60;
                monthCalendar1.Width = Screen.PrimaryScreen.Bounds.Width / 3 + 100;
                label3.Left = monthCalendar1.Width / 2;
                label3.Top = (panel3.Height - monthCalendar1.Height - label3.Height) / 2 +10;
                monthCalendar1.Left = button3.Width + 10;
                monthCalendar1.Top = label3.Bottom - 15;

                ListBox1.Height = panel3.Height - label4.Height - 50;
                ListBox1.Width = Screen.PrimaryScreen.Bounds.Width - monthCalendar1.Width - button3.Width - button2.Width - panel7.Width - 100;
                ListBox1.Left = monthCalendar1.Right + (Screen.PrimaryScreen.Bounds.Width - monthCalendar1.Width - button3.Width - button2.Width - ListBox1.Width - panel7.Width -40) / 2;

                label4.Left = ListBox1.Left + ListBox1.Width / 4;
                                
                listBox2.Top = panel3.Top - 5;
                label5.Top = listBox2.Top - label5.Height - 10;
                label17.Top = panel5.Top - label17.Height + 10;
                listBox2.Width = panel7.Width - 60;

                // Konfiguracja kalendarza
                monthCalendar1.MaxDate = DateTime.Now;
                monthCalendar1.SetSelectionRange(DateTime.Now.Subtract(new TimeSpan(1, 0, 0)), DateTime.Now);

                // Opróżnij listę kategorii
                ListBox1.Items.Clear();

                // Stwórz listę i zainicjalizuj ją numerami kanałów które chce zobaczyć użytkownik
                List<int> listaZadanychKanalow = checkedListBox1.CheckedIndices.Cast<int>().ToList();

                bool czyWypisanoOpcjeBrakuKategorii = false;

                // Dla każdej kategorii której numer kanału znajduje się na liście
                foreach (KeyValuePair<string, int> item in Pobieranie.Pobieranie.OdczytajKanaly().Where(item => listaZadanychKanalow.Contains(item.Value)))
                {
                    // Gdy trzeba wypisać wiadomości bez kategorii
                    if (item.Key == Operacje.brakKategorii)
                    {
                        // Jeśli opcja braku kategorii nie została jeszcze wypisana
                        if (!czyWypisanoOpcjeBrakuKategorii)
                        {
                            // Dodaj taką opcję
                            ListBox1.Items.Add(item.Key);

                            // Zaznacz że taka opcja została już dodana
                            czyWypisanoOpcjeBrakuKategorii = true;
                        }

                        // Weź następny obiekt z kolekcji
                        continue;
                    }

                    // Jeśli kategoria nie jest pusta, dodaj do listy
                    ListBox1.Items.Add(item.Key);
                }

                // Jeśli wypisano jakiekolwiek kategorie
                if (ListBox1.Items.Count > 0)
                {
                    // Wybierz pierwszą z nich
                    ListBox1.SelectedIndex = 0;
                }

                // Stwórz nową listę nazw kanałów i zainicjalizuj ją nazwami odpowiadającymi identyfikatorom z checkedListBox`a
                List<string> listaKanalow = listaZadanychKanalow.Select(item => Pobieranie.Pobieranie.kanaly[item].nazwa).ToList();

                // Ustaw źródło danych jako listę wiadomości istniejących na wybranych kanałach
                listBox2.DataSource = Operacje.WczytajNajnowsze(listaKanalow);

                // Wyświetlaj wiadomosc.tytul
                listBox2.DisplayMember = "tytul";

                // Wstępnie ustaw obrazek na logo interii, gdyby wiadomość go nie posiadała
                pictureBox1.Image = pictureBox1.InitialImage;

                // tabPage2 jest gotowy
                tabPage2.Tag = true;
            } 
        }

        /// <summary>
        /// EventHandler ustawiający tabPage2 jako wymagający ponownego malowania
        /// </summary>
        /// <param name="sender">auto</param>
        /// <param name="e">null</param>
        private void checkedListBox1_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            // trzeba zaktualizować wszystkie następne strony
            tabPage2.Tag = false;
            tabPage3.Tag = false;
            tabPage5.Tag = false;
        }

        /// <summary>
        /// Funkcja przygotowująca tabPage3 do wyświetlenia
        /// </summary>
        /// <param name="sender">tabPage3</param>
        /// <param name="e">null</param>
        private void tabPage3_Paint(object sender, PaintEventArgs e)
        {
            //jeśli tabPage3 nie jest gotowy
            if (!(bool)tabPage3.Tag)
            {
                //ustalenie rozmiarów interfejsu
                listBox3.Width = Screen.PrimaryScreen.Bounds.Width / 2 + 40;
                listBox3.Height = panel4.Height - 80;

                label10.Width = Screen.PrimaryScreen.Bounds.Width - listBox3.Width - 250;
                label10.Left = listBox3.Right + 20;
                label10.Height = listBox3.Height / 2 + 10;
                
                label7.Left = listBox3.Width / 2;

                label8.Left = listBox3.Width + label10.Width / 2 - pictureBox1.Width/13;
                listBox3.Top = label7.Bottom + 10;
                label10.Top = listBox3.Top;

                pictureBox1.Top = label10.Bottom + 10;
                pictureBox1.Width = label10.Width - 100;
                pictureBox1.Height = listBox3.Height - label10.Height - 10;
                pictureBox1.Left = label10.Left + (label10.Width - pictureBox1.Width)/2 + 10;

                // Wstępnie podgląd wiadomości jest pusty
                label10.Text = String.Empty;

                // Wstępnie obrazkiem wiadomości jest logo interii
                pictureBox1.Image = pictureBox1.InitialImage;

                // Wymuś ponowne przygotowanie pictureBox1
                pictureBox1.Invalidate();

                // Odłącz listBox3 od poprzedniego źródła danych
                listBox3.DataSource = null;

                // Opróżnij pozostałe dane
                listBox3.Items.Clear();

                // Stwórz listę nazw kanałów
                List<string> kanaly = new List<string>();

                // Dla indeksu każdego zaznaczonego kanału
                foreach (int item in checkedListBox1.CheckedIndices)
                {
                    // Dodaj do listy nazwę odpowiadającą indeksowi
                    kanaly.Add((string)(checkedListBox1.Items[item]));
                }

                // Stwórz listę wiadomości których tytuły mają być wyświetlone i przefiltruj ją przez wybrane wymagania
                List<Wiadomosc> wiadomosci = Operacje.WczytajTytuly(ListBox1.SelectedItem.ToString(),
                    monthCalendar1.SelectionRange.Start, monthCalendar1.SelectionRange.End, kanaly);

                // Jeśli żadna wiadomość nie spełnia kryteriów
                if (wiadomosci.Count == 0)
                {
                    // Wypisz informację dla użytkownika
                    listBox3.Items.Add(@"Brak wiadomości z wybranych kategorii w wybranym przedziale czasu.");
                }
                else
                {
                    // Jeśli istnieją takie wiadomości, ustaw je jako źródło danych listBox3
                    listBox3.DataSource = wiadomosci;

                    // Wyświetlaj wiadomosc.tytul
                    listBox3.DisplayMember = @"tytul";
                }

                // tabPage3 jest gotowy
                tabPage3.Tag = true;
            } 
        }

        /// <summary>
        /// Funkcja zmieniająca wyświetlane na tabPage3 dane, gdy wybierze się inną wiadomość
        /// </summary>
        /// <param name="sender">listBox3</param>
        /// <param name="e">null</param>
        private void listBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Ustaw na siebie zmienną przechowującą referencję do listBoxa z którego została wybrana wiadomość to wyświetlenia na tabPage5
            wybor = listBox3;
            try
            {
                label10.Text = ((Wiadomosc)wybor.SelectedItem).tytul;
                if (((Wiadomosc)wybor.SelectedItem).zdjecie=="")
                {
                    pictureBox1.Image = pictureBox1.InitialImage;
                }
                pictureBox1.ImageLocation = ((Wiadomosc)wybor.SelectedItem).zdjecie;
            }
            catch (InvalidCastException)
            {
                label10.Text = "";
                pictureBox1.Invalidate();
            }
            catch (NullReferenceException)
            {
                label10.Text = "";
                pictureBox1.Image = pictureBox1.InitialImage;
                pictureBox1.Invalidate();
            }
            tabPage5.Tag = false;
        }

        private void tabPage5_Paint(object sender, PaintEventArgs e)
        {
            //jeśli tabPage5 nie jest gotowy
            if (!(bool)tabPage5.Tag)
            {
                //ustalanie rozmiarow interfejsu 
                label11.Visible = true;
                label11.BringToFront();

                panel6.Width = Screen.PrimaryScreen.Bounds.Width / 4;
                panel6.Height = Screen.PrimaryScreen.Bounds.Height;
                panel6.Left = Screen.PrimaryScreen.Bounds.Width / 4 * 3 - 20;

                label12.Left = (Screen.PrimaryScreen.Bounds.Width - panel6.Width) / 3 + 20;
                label13.Left = (Screen.PrimaryScreen.Bounds.Width - panel6.Width) / 3 * 2;
           
                textBox2.Width = (Screen.PrimaryScreen.Bounds.Width - panel6.Width) / 2;
                textBox2.Height = Screen.PrimaryScreen.Bounds.Height / 4;

                textBox3.Width = Screen.PrimaryScreen.Bounds.Width - panel6.Width - 120;
                textBox3.Height = Screen.PrimaryScreen.Bounds.Height - textBox2.Height - 310;
                textBox3.Top = textBox2.Bottom + 10;               

                pictureBox2.Width = (Screen.PrimaryScreen.Bounds.Width - panel6.Width) / 2 - 140;
                pictureBox2.Height = textBox2.Height;
                pictureBox2.Left = textBox2.Right + 20;
                pictureBox2.ImageLocation = ((Wiadomosc)wybor.SelectedItem).zdjecie;
                pictureBox2.Image = pictureBox1.InitialImage;

                label16.Top = panel5.Top - 20;
                label18.Top = label16.Bottom; 
                
                listBox4.Width = panel6.Width - 80;
                listBox4.Top = label18.Bottom + 10;
                listBox4.Height = panel6.Height - label18.Bottom - (Screen.PrimaryScreen.Bounds.Height - panel5.Height)- 30;

                label15.Top = textBox3.Bottom + 10;
                label15.Left = textBox3.Left;
                label15.Width = textBox3.Width;
                label15.Height = 60;
                                
                Wiadomosc wybrana = (Wiadomosc)wybor.SelectedItem;
                // pole tytuł 
                label11.Text = wybrana.tytul.ToUpper();

                //pole kanał
                label14.Text = @"Kanał: " + wybrana.kanal;

                //pole kategoria
                if (wybrana.kategoria == null)
                {
                    label12.Text = @"Kategoria: " + Operacje.brakKategorii;
                }
                else
                {
                    label12.Text = @"Kategoria: " + wybrana.kategoria;
                }

                //pole data
                label13.Text = wybrana.data.ToString("f", CultureInfo.CurrentUICulture);

                //pole tresc
                textBox2.Text = wybrana.tresc;

                //pole artykul
                textBox3.Text = wybrana.artykul;

                //pole link
                label15.Text = wybrana.link;

                // Stwórz listę i zainicjalizuj ją numerami kanałów które chce zobaczyć użytkownik
                List<int> listaZadanychKanalow = checkedListBox1.CheckedIndices.Cast<int>().ToList();

                List<string> listaKanalow = listaZadanychKanalow.Select(item => Pobieranie.Pobieranie.kanaly[item].nazwa).ToList();

                listBox4.DataSource = Operacje.WczytajNajnowsze(listaKanalow);
                listBox4.DisplayMember = "tytul";

                // Resetowanie label15
                label15.LinkVisited = false;

                // tabPage5 jest gotowy
                tabPage5.Tag = true;
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // trzeba zaktualizować tabPage 3
            tabPage3.Tag = false;
            // trzeba zaktualizować tabPage 5
            tabPage5.Tag = false;
        }

        private void listBox2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListBox lista = (ListBox)sender;
            tabControl1.SelectedIndex = 3;
            wybor = lista;
            tabPage5.Tag = false;
            tabPage5_Paint(null, null);
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            // trzeba zaktualizować tabPage 3
            tabPage3.Tag = false;
            // trzeba zaktualizować tabPage 5
            tabPage5.Tag = false;
        }

        private void tabControl1_Deselecting(object sender, TabControlCancelEventArgs e)
        {
            poprzedniaZakladka = tabControl1.SelectedIndex;
            // Jeśli nie zaznaczono żadnego kanału
            if (checkedListBox1.CheckedItems.Count == 0)
            {
                MessageBox.Show(@"Wybierz przynajmniej jedną kategorię!", @"Błąd!",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tabControl1.SelectedIndex = 0;
                e.Cancel = true;
            }
        }

        private void label15_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            label15.LinkVisited = true;
            System.Diagnostics.Process.Start(((Label)sender).Text);
        }

        private void tabControl1_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (poprzedniaZakladka<=1 && e.TabPageIndex>=3 && (listBox3.Items.Count == 0 || !(bool)tabPage3.Tag))
            {
                MessageBox.Show(@"Najpierw wypełnij poprzednie pola!", @"Błąd!",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tabControl1.SelectedIndex = poprzedniaZakladka;
                return;
            }
            if (e.TabPageIndex == 3 && (listBox3.SelectedItem == @"Brak wiadomości z wybranych kategorii w wybranym przedziale czasu." || listBox3.SelectedItem == null))
            {
                 MessageBox.Show(@"Wypełnij najpierw poprzednie zakładki!", @"Błąd!",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tabControl1.SelectedIndex = poprzedniaZakladka;
                return;
            }
        }

    }
}
