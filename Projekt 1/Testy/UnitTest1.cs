﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using BazaDanych;
using System.Text.RegularExpressions;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Data.Entity;

namespace Testy
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestPobierzHtmlStrony()
        {
            Pobieranie.Pobieranie.glownyAdresURL = "http://rss.interia.pl";
            string url = Pobieranie.Pobieranie.glownyAdresURL;

            string html = Pobieranie.Pobieranie.PobierzHTMLStrony(url, false);
            StringAssert.StartsWith(html, "<?xml version=\"1.0\" encoding=\"iso-8859-2\" ?>");
            StringAssert.EndsWith(html, "</html>");
        }

        [TestMethod]
        public void TestPobraneAdresyURL()
        {
            Pobieranie.Pobieranie.glownyAdresURL = "http://rss.interia.pl";
            const string wzor1 = @"^http://kanaly.rss.interia.pl/\S\w*\.xml$"; //wzory pobranych linkow
            const string wzor2 = @"^http://[a-z/\.\-0-9]*/feed$";

            Pobieranie.Pobieranie.NowePobieranieKanalow();
            List<Pobieranie.Pobieranie.Kanal> kanaly = Pobieranie.Pobieranie.kanaly;

            foreach (var kanal in kanaly)
            {
                bool test;
                if (Regex.IsMatch(kanal.adresURL, wzor1) || Regex.IsMatch(kanal.adresURL, wzor2)) //sprawdza poprawnosc linku ze wzorami
                    test = true;
                else
                    test = false;

                Assert.IsTrue(test);
            }
        }

        [TestMethod]
        public void TestDeserializujWiadomosc()
        {
            XDocument xDoc = new XDocument(new XElement("item",                           //tworzy xml dla testu
                                                new XElement("title", "tytul"),
                                                new XElement("link", "http://link.pl"),
                                                new XElement("description", "tresc"),
                                                new XElement("category", "kategoria"),
                                                new XElement("pubDate", "Sun, 23 Nov 2014 09:33:00 GMT"),
                                                new XElement("guid", "guid")));
            xDoc.Save("xmlTest");   //zapisuje xml do pliku

            XmlDocument kanal = new XmlDocument();
            kanal.Load("xmlTest");

            XmlNodeList wiadomosciXML = kanal.GetElementsByTagName("item");

            Wiadomosc wiadomosc = Pobieranie.Pobieranie.DeserializujWiadomość(wiadomosciXML.Item(0));
            StringAssert.Contains(wiadomosc.tytul, "tytul");
            StringAssert.Contains(wiadomosc.link, "http://link.pl");
            StringAssert.Contains(wiadomosc.tresc, "tresc");
            StringAssert.Contains(wiadomosc.kategoria, "kategoria");
            StringAssert.Contains(wiadomosc.guid, "guid");
        }

        [TestMethod]
        public void TestPobierzArtykuł()
        {
            Pobieranie.Pobieranie.glownyAdresURL = "http://rss.interia.pl";
            XmlDocument kanalRSS = new XmlDocument();

            Pobieranie.Pobieranie.NowePobieranieKanalow();

            string adresURL = Pobieranie.Pobieranie.kanaly[0].adresURL;

            kanalRSS.Load(adresURL);

            XmlNodeList wiadomosciXML = kanalRSS.GetElementsByTagName("item");

            Wiadomosc wiadomosc = Pobieranie.Pobieranie.DeserializujWiadomość(wiadomosciXML.Item(0));

            wiadomosc = Pobieranie.Pobieranie.PobierzArtykuł(wiadomosc);

            Assert.IsFalse(wiadomosc.artykul.Contains("<"));
        }

        [TestMethod]
        public void TestPowtorzenWBazie()
        {
            List<Wiadomosc> listaWiadomosci = new List<Wiadomosc>();

            using (var db = new WiadomoscContext())
            {
                var lista = from x in db.Wiadomosci
                            group x by x.guid into grupa
                            select new { guid = grupa.Key, licznosc = grupa.Count() }; //zlicza wiadomosci o tym samym guid

                foreach (var item in lista)
                {
                    Assert.IsTrue(item.licznosc == 1); //jesli dla wszystkich licznosc=1 znaczy ze nie ma powtorzen
                }
            }
        }

        [TestMethod]
        public void TestZapisuDoBazy()
        {
            Wiadomosc wiadomosc = new Wiadomosc(); //wiadomosc testowa
            wiadomosc.kategoria = "test";
            wiadomosc.data = DateTime.Now;
            wiadomosc.guid = "12345";

            Operacje.Zapisz(wiadomosc); //zapisuje wiadomosc do bazy

            Assert.IsTrue(Operacje.CzyWiadomoscIstniejeWBazie(wiadomosc.guid)); //sprawdza czy zostala zapisana

            using (var db = new WiadomoscContext())
            {
                var test = (from x in db.Wiadomosci where x.guid == "12345" select x.WiadomoscId).ToList(); //odnajduje Id wiadomosci
                wiadomosc = db.Wiadomosci.Find(test[0]); //znajduje wiadomosc po Id
                db.Wiadomosci.Remove(wiadomosc); //usuwa wiadomosc
                db.SaveChanges();
            }
        }
    }
}
