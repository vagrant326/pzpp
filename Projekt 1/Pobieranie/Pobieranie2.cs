﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using BazaDanych;
using System.Xml;
using HtmlAgilityPack;
using System.Net;
using System.IO;

namespace Pobieranie
{
    static public partial class Pobieranie
    {
        /// <summary>
        /// Przyjmuje jedną niepełną Wiadomosc i uzupełnia w niej treść artykułu.
        /// </summary>
        /// <param name="wiadomosc">"Wiadomosc do której należy dopisać treść artykułu."</param>
        /// <exception cref="ArgumentOutOfRangeException">Nie udało się dopasować żadnego XPath</exception>
        /// <exception cref="WebException">Nie udało się wczytać treści strony</exception>
        /// <returns>Wiadomosc z dodaną treścią artykułu.</returns>
        public static Wiadomosc PobierzArtykuł(Wiadomosc wiadomosc)
        {
            // SB by wydajnie dopisywać treść
            StringBuilder tekst = new StringBuilder();

            // Dokument z treścią strony
            var strona = new HtmlAgilityPack.HtmlDocument();

            // Wczytaj treść strony pod adresem do którego wskazywał RSS, użyj kodowania UTF-8.
            strona.LoadHtml(PobierzHTMLStrony(wiadomosc.guid, true));

            // Inicjalizuj listę XPath
            List<string> xPath = new List<string>
            {
                "/html[1]/body[1]//div[@itemprop='articleBody']/p",
                "/html[1]/body[1]//div[@itemprop='articleBody']",
                "/html[1]/body[1]//div[contains(@class,'textContent')]//p",
                "/html[1]/body[1]//div[contains(@class,'textContent')]",
                "/html[1]/body[1]//div[@class='albumDescription description']/div[@class='content']//p",
                "/html[1]/body[1]//div[@class='albumDescription description']/div[@class='content']",
                "/html[1]/body[1]//div[@class='photoDescription description']/div[@class='content']//p",
                "/html[1]/body[1]//div[@class='photoDescription description']/div[@class='content']",
                "/html[1]/body[1]//div[@id='intertext1']",
                "/html[1]/body[1]//div[@class='borderContainer']/div[@class='description']/div/div"
            };

            // Iterator
            int i = 0;

            // Stwórz pustą kolekcję węzłów
            HtmlNodeCollection wezly = null;

            // Tak długo aż nie zapełni się kolekcja
            while (wezly == null)
            {
                try
                {
                    // Spróbuj wczytać węzły na podstawie i-tego XPath
                    wezly = strona.DocumentNode.SelectNodes(xPath[i++]);
                }
                // Jeśli podany XPath zwróci błąd
                catch (System.Xml.XPath.XPathException)
                {
                    // Wypróbuj następny XPath
                }
            }

            // Dla każdego elementu <div class="*lead content*"> lub <div itemprop="articleBody">
            foreach (HtmlNode node in wezly)
            {
                // Usuwanie reklamy

                // Stwórz zmienną dla węzłów w tekście
                HtmlNode child;

                // Tak długo jak w tekście będą węzły <div>
                while (( child = node.ChildNodes.FindFirst("div")) != null)
                {
                    // Usuń ten węzeł z tekstu
                    child.Remove();
                }

                // Dopisz znaleziony tekst do treści artykułu
                tekst.Append(node.InnerText.Trim() + @" ");
            }

            // Dopisz do obiektu wiadomości treść artykułu zamieniając znaczniki &nbsp na spację;
            wiadomosc.artykul = tekst.Replace("&nbsp;", " ").ToString();

            // Zwróc gotową wiadomość
            return wiadomosc;
        }

        /// <summary>
        /// Tnie cały XML na listę obiektów typu Wiadomosc.
        /// </summary>
        /// <param name="adresURL">Adres URL kanalu RSS</param>
        /// <returns>Lista kompletnych wiadomosci</returns>
        static internal List<Wiadomosc> DeserializujKanał(string adresURL)
        {
            // Kolekcja obiektow wiadomosci kanalu RSS
            List<Wiadomosc> listaWiadomości = new List<Wiadomosc>();

            // Dokument XML kanalu RSS
            XmlDocument kanalRSS = new XmlDocument();
            
            // Stwórz nowe zadanie o podanym adresie
            WebRequest zadanie = WebRequest.Create(adresURL);

            // Ustaw limit czasu żądania na 10 minut
            zadanie.Timeout = 600000;

            // Stwórz obiekt odpowiedzi
            WebResponse odpowiedz=null;

            // Spróbuj
            try
            {
                // Wysłać żądanie
                odpowiedz = zadanie.GetResponse();
            }
            // Jeśli żądanie zwróci wyjątek
            catch (WebException e)
            {
                // Jeśli przekroczono czas operacji
                if (e.Message == "Upłynął limit czasu operacji")
                {
                    MessageBox.Show(e.Message + @" Sprawdź obciążenie łącza i spróbuj ponownie.",@"Błąd!",
                        MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
            }

            // Jeśli serwer poprawnie zwrócił odpowiedź
            if (odpowiedz != null)
            {
                // Stwórz strumień danych na podstawie odpowiedzi serwera
                Stream strumien = odpowiedz.GetResponseStream();

                // Jeśli strumień nie jest pusty wczytaj go do obiektu XmlDocument
                if (strumien != null) kanalRSS.Load(strumien);
                // Jeśli jest pusty, zwróć wyjątek
                else throw new ArgumentNullException();
            }
            // Jeśli serwer nie zwrócił poprawnie odpowiedzi, zwróć wyjątek
            else throw new ArgumentNullException();


            // Pobierz wiadomosci z dokumentu XML
            XmlNodeList wiadomosciXML = kanalRSS.GetElementsByTagName("item");

            // Dla każdego węzła w wiadomoscXML
            for (int i = 0; i < wiadomosciXML.Count; i++)
            {
                // Stwórz nowy obiekt wiadomości i wypełnij go treścią wczytaną z węzła XML
                Wiadomosc wiadomosc = DeserializujWiadomość(wiadomosciXML.Item(i));

                // Jeśli wiadomość o takim GUID nie istnieje jeszcze w bazie
                if (!Operacje.CzyWiadomoscIstniejeWBazie(wiadomosc.guid))
                {
                    // Spróbuj
                    try
                    {
                        // Dopisać do wiadomości odpowiadający jej artykuł
                        wiadomosc = PobierzArtykuł(wiadomosc);
                    }
                    catch (WebException e)
                    {
                        MessageBox.Show("Nie udało się pobrać artykułu dla wiadomości \"" + wiadomosc.tytul + "\":\n" + e.Message, @"Błąd!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        wiadomosc.artykul = "Nie udało się pobrać artykułu" + e.Message;
                    }
                    catch (ArgumentOutOfRangeException e)
                    {
                        MessageBox.Show(@"Błąd w wiadomosci " + wiadomosc.link + "\n" + wiadomosc.tytul, @"Błąd!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        wiadomosc.artykul = "Nie udało się odczytać artykułu" + e.Message;
                    }

                    // Usuń oznaczenia HTML z opisu Wiadomości
                    var kodHTML = new HtmlAgilityPack.HtmlDocument();
                    kodHTML.LoadHtml(wiadomosc.tresc);
                    wiadomosc.tresc = kodHTML.DocumentNode.InnerText;

                    // Jeśli wczytana data nie jest poprawna (np. w kanale ProgramTV)
                    if (wiadomosc.data.Year == 1)
                    {
                        // Przypisz jej datę i godzinę wczytania do programu
                        wiadomosc.data = DateTime.Now;
                    }

                    // Dodaj gotową wiadomość do kolekcji
                    listaWiadomości.Add(wiadomosc);
                }
            }
            //Zwróć gotową kolekcję wiadomości
            return listaWiadomości;
        }

        /// <summary>
        /// Tnie obiekt węzła w dokumencie XML na obiekt klasy
        /// </summary>
        /// <param name="wezelXMLWiadomosci">Obiekt XmlNode węzła wiadomości</param>
        /// <returns>Pojedyncza wiadomosc bez tresci artykulu</returns>
        static public Wiadomosc DeserializujWiadomość(XmlNode wezelXMLWiadomosci)
        {
            // Czytnik wezlow XML
            XmlNodeReader czytnikWezlowXML = new XmlNodeReader(wezelXMLWiadomosci);

            // Serializator XML
            XmlSerializer serializator = new XmlSerializer(typeof(Wiadomosc));

            // Deserializuj XML na obiekt Wiadomość
            Wiadomosc wiadomosc = (Wiadomosc)serializator.Deserialize(czytnikWezlowXML);

            // Zamknij strumień czytnika wezłów XML
            czytnikWezlowXML.Close();

            // Jeśli węzeł XML posiada element "enclosure", dopisz jego atrybut URL jako link do zdjęcia
            wiadomosc.zdjecie = (wezelXMLWiadomosci["enclosure"] != null) ? wezelXMLWiadomosci["enclosure"].GetAttribute("url") : "";

            // Zwróć wczytaną treść wiadomości
            return wiadomosc;
        }

        /// <summary>
        /// Pośredniczy w przesłaniu listy kategorii znajdujących się w wybranych kanałach z BazaDanych do Interfejs.
        /// Pobiera z BazaDanych String - nazwa kategorii, String - nazwa kanału
        /// </summary>
        /// <returns>
        /// Lista par [String - nazwa kategorii, Int - numer kanału na liście kanałów]
        /// </returns>
        static public List<KeyValuePair<string, int>> OdczytajKanaly()
        {
            // Pierwszy string to nazwa kategorii a drugi to nazwa kanału w którym ta kategoria jest dostępna
            List<KeyValuePair<string, string>> dane = Operacje.OdczytajKategorie();

            // Stwórz listę wszystkich dostępnych kanałów
            List<string> nazwyKanalow = kanaly.Select(item => item.nazwa).ToList();

            // Stwórz listę par [Nazwa kategorii, Numer kanału w którym występuje]
            List<KeyValuePair<string, int>> wynik = new List<KeyValuePair<string, int>>();

            // Dla każdej pary w liście z bazy danych
            foreach (var item in dane)
            {
                // Dodaj do zwracanej listy parę [nazwa kategorii, numer kanału z własnych zasobów]
                wynik.Add(new KeyValuePair<string, int>(item.Key,nazwyKanalow.IndexOf(item.Value)));
            }

            // Zwróć gotową listę
            return wynik;
        }
    }
}
