﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pobieranie
{
    using BazaDanych;

    public class WatekPobraniaKanalu
    {
        public WatekPobraniaKanalu(string adresURL, string nazwa)
        {
            Task.Factory.StartNew(() => ProceduraWatku(adresURL, nazwa));
        }

        private void ProceduraWatku(string adresURL, string nazwa)
        {
            List<Wiadomosc> wiadomosciZKanalu = Pobieranie.DeserializujKanał(adresURL);
            for (int j = 0; j < wiadomosciZKanalu.Count; j++)
            {
                wiadomosciZKanalu[j].kanal = nazwa;
                Operacje.Zapisz(wiadomosciZKanalu[j]);
            }
        }
    }
}
