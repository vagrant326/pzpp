﻿using System.Collections.Generic;
using System.Text;
using System.Net;
using HtmlAgilityPack;
using System;

namespace Pobieranie
{
    /// <summary>
    /// Główna klasa obsługująca wszelkie aspekty aplikacji wymagające połączenia z Internetem.
    /// </summary>
    static public partial class Pobieranie
    {
        /// <summary>
        /// Adres URL strony WWW której adresy (kanały RSS) mają być pobrane
        /// </summary>
        static public string glownyAdresURL;

        /// <summary>
        /// Kolekcja pobranych kanałów RSS
        /// </summary>
        public static List<Kanal> kanaly { get; private set; }

        /// <summary>
        /// Struktura grupująca dane opisujące jeden kanał
        /// </summary>
        public struct Kanal
        {
            public string adresURL;
            public string nazwa;
        }

        /// <summary>
        /// Główna procedura pobierająca wiadomości
        /// </summary>
        public static void Pobierz()
        {
            // Wypełnij kolekcję kanałów
            NowePobieranieKanalow();

            // Inicjalizuj tablicę wątków
            WatekPobraniaKanalu[] watkiPobieraniaKanalow = new WatekPobraniaKanalu[kanaly.Count];

            // Dla każdego kanału...
            for (int i = 0; i < kanaly.Count; i++) //do testów
            {
                // ...stwórz nowy wątek
                watkiPobieraniaKanalow[i] = new WatekPobraniaKanalu(kanaly[i].adresURL, kanaly[i].nazwa); 
            }
        }

        /// <summary>
        /// Funkcja wypełniająca listę kanałów obiektami na podstawie strony pod adresem podanym w configu.
        /// </summary>
        public static void NowePobieranieKanalow()
        {
            // Inicjalizuj listę kanałow
            kanaly = new List<Kanal>();

            // Dokument z treścią strony
            HtmlDocument strona = new HtmlDocument();

            // Wczytaj stronę pod adresem podanym w zmiennej, użyj kodowania iso-8859-2
            strona.LoadHtml(PobierzHTMLStrony(glownyAdresURL, false));

            // Dla każdego elementu <tr> w <div class="MainPolecane"><table>
            foreach (HtmlNode wezel in strona.DocumentNode.SelectNodes("//div[@class='MainPolecane']/table/tr"))
            {
                // Inicjalizuj nowy kanał
                Kanal wczytany = new Kanal();


                // Z danego <tr> wczytaj <td class="tdPolecane01">
                HtmlNode podWezel = wezel.SelectSingleNode("td[@class='tdPolecane01']");

                // Nazwa kanału to zawartość tego <td>
                wczytany.nazwa = podWezel.InnerText;


                // Z danego <tr> wczytaj <td class="tdPolecane02">
                podWezel = wezel.SelectSingleNode("td[@class='tdPolecane02']");

                // Adres tego kanału to zawartość tego <td>
                wczytany.adresURL = podWezel.InnerText;

                if (wczytany.nazwa != "Polska Lokalna")
                {
                    // Dodaj gotowy kanał do kolekcji
                    kanaly.Add(wczytany);
                }
            }
        }

        /// <summary>
        /// Funkcja pobierajaca kod HTML podanej strony www
        /// </summary>
        /// <param name="url">Adres URL strony www ktorej kod ma byc zwrocony</param>
        /// <param name="utf8">Czy uzyc kodowania UTF-8. Jeśli nie, używa ISO 8859-2</param>
        /// <returns>Kod HTML podanej strony www</returns>
        public static string PobierzHTMLStrony(string url, bool utf8)
        {
            // Utwórz nowego WebClient`a z kodowaniem UTF-8 lub ISO-8859-2
            ZmodyfikowanyClient klientWWW = new ZmodyfikowanyClient {Encoding = utf8 ? Encoding.UTF8 : Encoding.GetEncoding("iso-8859-2")};

            // Zwróć treść strony o podanym adresie bezwzględnym w formie tekstu
            return klientWWW.DownloadString(new Uri(url, UriKind.Absolute));
        }
    }

    /// <summary>
    /// Klasa dziedzicząca z WebClient, nadpisująca maksymalny czas wykonania operacji
    /// </summary>
    public class ZmodyfikowanyClient : WebClient
    {
        /// <summary>
        /// Maksymalny czas po którym Client przerwie działanie
        /// </summary>
        public int czasOperacji { get; set; }

        /// <summary>
        /// Konstuktor domyślny automatycznie ustawia czasOperacji na 10 minut.
        /// </summary>
        public ZmodyfikowanyClient()
        {
            czasOperacji = 600000;
        }

        /// <summary>
        /// Konstruktor pozwalający ustawić pole czasOperacji na żądaną wartość
        /// </summary>
        /// <param name="czas">Ilość milisekund na wykonanie żądania</param>
        public ZmodyfikowanyClient(int czas)
        {
            czasOperacji = czas;
        }

        /// <summary>
        /// Funkcja zwracająca obiekt WebRequest z podanym adresem i powiększonym maksymalnym czasem operacji
        /// </summary>
        /// <param name="adres">Adres strony która ma zostać pobrana</param>
        /// <returns></returns>
        protected override WebRequest GetWebRequest(Uri adres)
        {
            // Stwórz WebRequest o podanym adresie korzystając z WebClient.GetWebRequest(string)
            WebRequest zadanie = base.GetWebRequest(adres);

            // Jeśli nie uda się stworzyć żądania, zwróć wyjątek
            if (zadanie == null) throw new NullReferenceException();

            // Ustaw własny czas operacji
            zadanie.Timeout = czasOperacji;

            // Zwróć żądanie
            return zadanie;
        }
    }
}
