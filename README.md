# README #

Szybkie podsumowanie projektu.

### Zadanie ###

* Pobrać http://rss.interia.pl/index.html
* Wczytać adresy kanałów
* Pobrać stronę każdego kanału
* Wczytać dane każdej wiadomości
* Pobrać stronę spod linku w danych wiadomości
* Zapisać treść artykułu do bazy danych (bez obrazków, reklam itp.)

### Założenia ###

* Używamy ORM, zapewne Entity Framework
* Komentujemy wszystko co się da
* RegEx do cięcia HTML
* Stworzyć jak najwięcej testów
* Kod powinien być idiotoodporny (sprawdzanie danych wejściowych, Exception`y)
* Całość powinna składać się z modułów które łatwo wymieniać
* Na koniec sprawozdanie

### Współpraca ###
Komunikujemy się przez Facebooka, maila, wiadomości na BitBucket, Skype (w tym konferencje). Unikajmy wiadomości w kodzie.